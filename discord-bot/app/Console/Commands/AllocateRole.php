<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class AllocateRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'role:allocate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Allocate discord server role based on the event reactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $authcode = env('BOT_AUTH_CODE');
        $channelsnowflake = env('EVENTS_CHANNEL_ID');

        $url = 'https://discord.com/api/channels/' . $channelsnowflake . '/messages';

        $tokenHeader = 'Authorization: Bot ' . $authcode;
        $userAgentHeader = 'User-Agent: DiscordBot (https://morganwel.ch, 1)';


        $ch = curl_init();

        $headers = [
            $tokenHeader,
            $userAgentHeader
        ];

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $results = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($results, true);

        $lastMessage = $results[0];

        $eventName = $lastMessage['embeds'][0]['title'];

        $accepted = $lastMessage['embeds'][0]['fields'][1]['value'];
        $accepted = explode("\n", $accepted);
        $accepted[0] = str_replace('>>> ', '', $accepted[0]);

        $guildId = env('GUILD_ID');

        $url = 'https://discord.com/api/guilds/' . $guildId;

        $ch = curl_init();

        $headers = [
            $tokenHeader,
            $userAgentHeader
        ];

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $results = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($results, true);

        $url = 'https://discord.com/api/guilds/' . $guildId . '/members?limit=1000';

        $ch = curl_init();

        $headers = [
            $tokenHeader,
            $userAgentHeader
        ];

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $results = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($results, true);

        $ids = [];
        $removeIds = [];

        foreach($results as $user) {
            $username = $user['user']['username'];
            $nickname = $user['nick'] ?? '';
            if (in_array($username, $accepted) || in_array($nickname, $accepted)) {
                $ids[] = $user['user']['id'];
            } else {
                $removeIds[] = $user['user']['id'];
            }
        }

        $url = 'https://discord.com/api/guilds/' . $guildId . '/roles';

        $ch = curl_init();

        $headers = [
            $tokenHeader,
            $userAgentHeader
        ];

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $results = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($results, true);

        $numberOfRoles = sizeof($results);

        $found = false;
        $roleId = 0;

        foreach ($results as $role) {
            if ($role['name'] == $eventName) {
                $found = true;
                $roleId = $role['id'];
            }
        }

        if (!$found) {
            $headers = [
                $tokenHeader,
                $userAgentHeader,
                "Content-Type: application/json"
            ];

            $data = json_encode([
                'name' => $eventName,
                'hoist' => true,
                'mentionable' => true,
                'color' => 4278190080
            ]);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            $results = curl_exec($ch);
            curl_close($ch);
            $results = json_decode($results, true);
        }

        foreach ($ids as $id) {
            $url = 'https://discord.com/api/guilds/' . $guildId . '/members/' . $id . '/roles/' . $roleId;

            $headers = [
                $tokenHeader,
                $userAgentHeader,
                'Content-Length: 0'
            ];

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

            $results = curl_exec($ch);
            curl_close($ch);
        }

        foreach ($removeIds as $id) {
            $url = 'https://discord.com/api/guilds/' . $guildId . '/members/' . $id . '/roles/' . $roleId;

            $headers = [
                $tokenHeader,
                $userAgentHeader,
                'Content-Length: 0'
            ];

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

            $results = curl_exec($ch);
            curl_close($ch);
        }
    }
}
